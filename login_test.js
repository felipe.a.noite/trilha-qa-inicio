Feature('login');

Scenario('Login com Sucesso',  ({ I }) => { 

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#user', 'felipe.a.noite@gmail.com')
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('Login realizado', 5)

}).tag('Cenario1')

Scenario('Tentando logar digitando apenas o email',  ({ I }) => { 

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#user', 'felipe.a.noite@gmail.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida', 5)

}).tag('Cenario2')

Scenario('Tentando logar sem digitar email e senha',  ({ I }) => { 

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido', 5)

}).tag('Cenario3')

Scenario('Tentando logar digitando apenas a senha',  ({ I }) => { 

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido', 5)

}).tag('Cenario4')